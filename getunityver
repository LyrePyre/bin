#!/bin/bash

function getUnityVerNoGit() {
  e=0
  find "$1" -path '*ProjectSettings/ProjectVersion.txt' | while read f; do
    echo "${f}"
    grep -Eo -m1 --color=auto '([1-5]|20[0-9]{2})\.([0-9]+)\.([0-9]+)(f[0-9]+)?' "$f"
    e+=$?
  done

  if [[ $e -ne 0 ]]; then
    >&2 echo "wtf '$1'??  Broken script, or broken you?  e=$e"
    return 1
  fi
}

UNIQ='uniq -w47'

if !(echo "MACS SUCK" | $UNIQ &>/dev/null); then
  UNIQ='cat'
fi

function getUnityVerWithGit() { # NEW - uses git grep
  pushd "$1" 1>/dev/null
  if [[ $? -ne 0 ]]; then
    return 1
  fi

  if !(git status &>/dev/null); then # fallback to git-less impl.
    popd 1>/dev/null
    getUnityVerNoGit "$1"
    return 1
  fi

  declare -a revs
  let i=0
  shift

  while [[ -n "$1" ]]; do
    if (git rev-parse "$1" &>/dev/null); then
      revs+=("$1")
    elif (git branch -r | grep "origin/$1" &>/dev/null); then
      revs+=("$(git branch -r --list origin/$1 | head -n1)")
    else
      break
    fi

    let i=i+1
    shift
  done

  git grep -Eo --color=never ' ([1-5]|20[0-9]{2})\.([0-9]+)\.([0-9]+)(f[0-9]+)?( \([a-f0-9]{12}\))?' ${revs[@]} -- ':!*Assets/*' '*ProjectSettings/ProjectVersion.txt' \
    | $UNIQ \
    | grep -E --color=auto ' ([1-5]|20[0-9]{2})\.([0-9]+)\.([0-9]+)(f[0-9]+)?'

  popd 1>/dev/null
  return $i
}

if [[ $# -eq 0 ]]; then
  getUnityVerWithGit "../${PWD##*/}"
  exit
fi

while [[ -n "$1" ]]; do
  if [[ -d "$1" ]]; then
    getUnityVerWithGit $@
    shift $?
  elif [[ -r "$1" ]]; then
    "$0" $(grep -vE '^(\s*#|$)' "$1")
    shift
  else
    getUnityVerWithGit "../${PWD##*/}" $@
    shft=$?
    if [[ $shft -eq 0 ]]; then
      >&2 echo "'$1' is not a directory, list file, or local git revision."
      shift
    else
      shift $shft
    fi
  fi
done
